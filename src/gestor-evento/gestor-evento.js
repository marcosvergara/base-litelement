import { LitElement, html } from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement {

    static get properties() {
        return{
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h1>Gestor Evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e) {
        console.log("Entra en processEvent");
        console.log(e);

        this.shadowRoot.getElementById("receiver").course = e.detail.course;    //el de la izquierda es la propiedad, la derecha el evento
        this.shadowRoot.getElementById("receiver").year = e.detail.year;    //el de la izquierda es la propiedad, la derecha el evento
    }
}

customElements.define('gestor-evento', GestorEvento)
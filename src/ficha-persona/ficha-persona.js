import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {
    
    static get properties() {   //esto entre muchisimas comillas sería el "MODELO" del MVC
        //[] es un array
        //{} es un object

        return{ //vamos a ver qué objeto devolvemos
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        };
    }

    constructor() {
        super();  //si estamos en relaciones de herencia; el super llama a la clase "padre", es decir, llama al constructor de LitElement

        this.name = "Prueba Nombre";
        this.yearsInCompany = 30;
        this.updatePersonInfo();
    }

    render() {  //esta función genera una especie de revisión del código html que se genera; seria la "VISTA" del modelo MVC; este es un motor de plantillas de LitElement
        return html`
            <div>
                <label>Nombre completo: </label>
                <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label>Años en la empresa: </label>
                <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" value="${this.personInfo}" disabled></input>
            </div>
        `;
    }

    updated(changedProperties) {  //función del ciclo de vida de LitElement; se llama cuando se ha actualizado y pintado (pasado render) del webcomponent
        console.log("Entra en la función de LitElement updated");
        //changedProperties.forEach((oldValue, propName) => {
        //console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
        //});

        if (changedProperties.has("name")) {
            console.log("Propiedad name cambia valor, anterior era " + changedProperties.get("name") + " y nuevo es " + this.name);
        }

        if (changedProperties.has("yearsInCompany")) {
            console.log("Propiedad yearsInCompany cambia valor, anterior era " + changedProperties.get("yearsInCompany") 
            + " y nuevo es " + this.yearsInCompany);
            this.updatePersonInfo();  //así sólo la llamo si cambia sólo la propiedad yearsInCompany
        }
    }

    updateName(e) {
        console.log("Entra en la función updateName");
        this.name = e.target.value;    //el e.target de un evento input completo >> queremos el value de ese input
    }

    updateYearsInCompany(e) {
        console.log("Entra en la función updateYearsInCompany");
        this.yearsInCompany = e.target.value;  
        console.log("this.yearsInCompany es " + this.yearsInCompany);
    }

    updatePersonInfo () {
        console.log("Entro en la función updatePersonInfo");
        console.log("yearsInCompany vale " + this.yearsInCompany);

        if (this.yearsInCompany >= 7) {
            this.personInfo = "lead";
        } else if (this.yearsInCompany >=5 ) {
            this.personInfo = "senior";
        } else if (this.yearsInCompany >=3) {
            this.personInfo = "team";
        } else {
            this.personInfo = "junior";
        }
    }
}

customElements.define('ficha-persona', FichaPersona)

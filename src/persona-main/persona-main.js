import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    static get properties() {
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: "Gaspar de Jovellanos", 
                yearsInCompany: 10,
                profile: "Lorem ipsum dolor sit amet.",
                photo: {
                    src: "./img/jovellanos.jpg",
                    alt: "Gaspar de Jovellanos"
                }
            }, {
                name: "Pedro R. de Campomanes",
                yearsInCompany: 2,
                profile: "Lorem ipsum.",                
                photo: {
                    src: "./img/campomanes.jpg",
                    alt: "Pedro R. de Campomanes"
                }
            }, {
                name: "Benito Jerónimo Feijóo",
                yearsInCompany: 5,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
                photo: {
                    src: "./img/feijoo.jpg",
                    alt: "Benito Jerónimo Feijóo"
                }
            }, {
                name: "Agustín de Argüelles",
                yearsInCompany: 3,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
                photo: {
                    src: "./img/arguelles.jpg",
                    alt: "Agustín de Argüelles"
                }
            }, {
                name: "Conde de Floridablanca",
                yearsInCompany: 7,
                profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut...",
                photo: {
                    src: "./img/floridablanca.jpg",
                    alt: "Conde de Floridablanca"
                }
            }
        ];

        this.showPersonForm = false;
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">        
            <h2 class="text-center">Los asturianos más influyentes de la Ilustración</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`
                    <persona-ficha-listado
                        fname="${person.name}" 
                        yearsInCompany="${person.yearsInCompany}"
                        profile="${person.profile}"
                        .photo="${person.photo}"
                        @delete-person="${this.deletePerson}"
                        @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>`
                )}  
                </div>
            </div>
            <div class="row">
                    <persona-form 
                        @persona-form-close="${this.personFormClose}" 
                        @persona-form-store="${this.personFormStore}"
                        class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
        `;
    }

    updated (changedProperties) {   //esta función "update" nos la da LitElement
        console.log("Entramos en la función nativa de LitElement updated porque ha cambiado el valor de showPersonForm");

        if (changedProperties.has("showPersonForm")) {
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            if (this.showPersonForm === true) {
                this.showPersonFormData();
            } else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                    "detail": {
                        people: this.people
                    }
                }
            ));
        }
    }

    showPersonFormData() {
        console.log("Al ser showPersonForm=true, entro en showPersonFormData");
        console.log("Mostrando formulario de alta de una persona");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList() {
        console.log("Al ser showPersonForm=false, entro en showPersonList");
        console.log("Mostrando listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    personFormClose() {
        console.log("Entro en personFormClose");
        console.log("Se ha cerrado el formulario de la persona");

        this.showPersonForm = false;
        console.log("El valor de showPersonForm de nuevo a: " + this.showPersonForm);
    }

    personFormStore(e) {
        console.log("Entro en personFormStore");
        console.log("Se va a almacenar o actualizar una persona");

        console.log("La propiedad name en person vale: " + e.detail.person.name);
        console.log("La propiedad profile en person vale: " + e.detail.person.profile);
        console.log("La propiedad yearsInCompany en person vale: " + e.detail.person.yearsInCompany);
        console.log("La propiedad editingPerson vale: " + e.detail.editingPerson);

        if (e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre: " + e.detail.person.name);
            
            /*let indexOfPerson = this.people.findIndex(
                person => person.name === e.detail.person.name
            );
            // indexOfPerson será -1 si no lo ha encontrado; si ha encontrado algo será 0 o mayor

            if (indexOfPerson >= 0) {
                console.log("Persona encontrada");
                this.people[indexOfPerson] = e.detail.person;
                console.log("Persona actualizada");
            }*/

            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            );

        } else {
            console.log("Se va a almacenar una persona nueva");
            //this.people.push(e.detail.person);   //con esto incorporo al array el nuevo elemento
            this.people = [...this.people, e.detail.person];    //se refiere al array entero pero con los elementos separados
            console.log("Persona almacenada");
        }

        this.showPersonForm = false;
    }

    deletePerson(e) {
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona con nombre: " + e.detail.name);

        this.people = this.people.filter(
            person => person.name != e.detail.name
        );
    }
    
    infoPerson(e) {
        console.log("infoPerson en persona-main");
        console.log("Se ha pedido más información de la persona de nombre: " + e.detail.name);

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        console.log(chosenPerson);    //ojo no concatenar!!! Si es un array no funciona
        
        let personToShow = {};
        personToShow.name = chosenPerson[0].name;
        personToShow.profile = chosenPerson[0].profile;
        personToShow.yearsInCompany = chosenPerson[0].yearsInCompany;

        this.shadowRoot.getElementById("personForm").person = personToShow;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
    }
}

customElements.define('persona-main', PersonaMain)
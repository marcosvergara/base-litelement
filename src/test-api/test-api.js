import { LitElement, html } from 'lit-element';

class TestApi extends LitElement {

    static get properties() {
        return{
            movies: {type: Array}    //tenemos que revisar la API para ver el formato de lo que nos devolverá la APIRest; es un array de objects
        }
    }

    constructor () {
        super();

        this.movies = [];   //inicializo el array para que no me dé undefined al principio
        this.getMovieData();   //las funciones que incluyo en el constructor se ejecutan al cargar la página, lo primero
    }

    render() {
        return html`
            ${this.movies.map(
                movie => html`<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `;
    }

    getMovieData() {
        console.log("Accedo a getMovieData");
        console.log("Obteniendo datos de las películas de la serie Star Wars");

        let xhr = new XMLHttpRequest();   //estándar para traerse datos del servidor (llamar a una API)
        //si ponemos xhr = new XMLHttpRequest(), se declará a nivel global
        
        xhr.onload = () => {
            if (xhr.status === 200) {   //el estado de la petición XMLHttpRequest está en status
                console.log("He lanzado la petición y el servidor me ha contestado con un " + xhr.status + ", es decir, la petición se ha completado correctamente");

                let APIResponse = JSON.parse(xhr.responseText);    //la respuesta a la llamada al XMLHttpRequest estará en responseText; es un string, un texto
                //¿cómo preparo ese string (un json) para entresacar la info? Con el parse de JSON.
                //console.log("xhr.responseText:" + xhr.responseText);    //con esto pinto todo el JSON

                this.movies = APIResponse.results;   //results es cómo está organizado el json que me devuelve esta API
            }
        };

        xhr.open("GET", "https://swapi.dev/api/films/");   //aquí ponemos la URL donde esté el backend disponible, el que nos digan ellos
        xhr.send();   //esta línea es en donde se hace la petición
        //en el Mozilla Development etc... está cómo mandar un POST
        console.log("Finaliza getMovieData");
    }
}

customElements.define('test-api', TestApi)